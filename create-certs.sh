#!/bin/bash

# docker network create nordcod-network

mv config/nginx.conf config/nginx.conf.bk
mv config/nginx.conf.initial config/nginx.conf
docker-compose up --build nginx -d # start nginx for acme challenge
docker-compose -f docker-compose-le.yaml up --build # create certs

docker stop nordcod-nginx # now use nginx with full config
mv config/nginx.conf config/nginx.conf.initial
mv config/nginx.conf.bk config/nginx.conf
